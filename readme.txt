=== Floating Menu Button Links ===
Contributors: blocksera, vijaydj
Donate link: https://floatingmenu.blocksera.com
Tags: button, floating button, menu, float link, conversion
Requires at least: 4.0
Tested up to: 5.1.1
Stable tag: 1.0
Requires PHP: 5.2.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Add Static floating button type Links to your web pages

== Description ==

This plugin lets you add static links as floating button on your webpages,

= Features =
* Control over your links and icons (font awesome)
* Modify Position, Size, Unit, Color(Icon, Background, Hover) of the Menu

== Installation ==

1. Upload the plugin and activate it (alternatively, install through the WP admin console)
3. Use the Settings->Plugin Name screen to configure the plugin
4. Start using the plugin by navigating to the Submenu "Floating Buttons" in your sidebar.

== Changelog ==

= 1.0 =
* Initial Version